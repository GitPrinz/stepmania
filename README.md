# StepMania Scripts

Some Scripts for my needs to read the Profiles and modify the "Preferred Song List" in Step Mania 5:

Current Features:

- Player Statistics
- Most Played Songs.
- Songs with Videos.

There is some Hardcode you have to change.
See the examples at the end of the files.

If you have helpful information or comments, please do not hesitate to contact me.

Used version is [StepMania 5.0.12](https://www.stepmania.com/)

## Disclaimer / Other Solutions

This is all experimental and only the current solution for myself.
It could overwrite your files (like Preferred Songs). So handle with care and backup.

I also found some more professional approaches in this [redit post](https://www.reddit.com/r/Stepmania/comments/gqih2j/exporting_statistics/):
- [EtternaGraph](https://github.com/kangalioo/etterna-graph) Show Statistics for Etterna. *This could be really helpful!*
- [StepMania Helper Program](https://www.stepmania.com/forums/general-stepmania/show/1881) to organize songs (closed source?)
- [padmiss-deamon](https://github.com/electromuis/padmiss-daemon) for special USB Sensors to Read Out Inputs?

Please let me know if there is more, I will link them.
If I find the time I will may adapt my scripts or change to the other solutions.
The best would be to do it inside StepMania, but that is to much for now ;)


## Overview

- [Profile.py](source/Profile.py): Access some of your Profile Information.
  - Most played Songs (Overall/Profile)
  - Preferred Songs
  - Based on: [ScanProfile.py](source/ScanProfile.py)
    - Song Statistics
    - Player Statistics
- [SongList.py](source/SongList.py): Handle Preferred Song List.
  - Add Most Played
  - Add Songs with Video
  - Based on: [ScanSongs.py](source/ScanSongs.py)  
  (Experimental file to edit your Preferred Songs. Not useful without changes.)
    - Get all Songs with Video
    - Sort your Preferred Songs
    - Clean Preferred Songs of Duplicates (They cause trouble in the SM5 Interface)


## Ideas

- [x] Get most played Songs of all Profiles
- [ ] Get Songs with highest Scores
- [ ] Get Songs for specific difficulty 