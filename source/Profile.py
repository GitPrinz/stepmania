from os import listdir
from os.path import exists, join as path_join

import numpy as np

from source.ScanProfile import get_player_statistics, get_song_statistics


def get_profile_names(profile_path: str = '../../Save/LocalProfiles') -> dict:
    """
    :return: dict with profile names and path
    :param profile_path: path to profile folder
    """

    profile_folders = listdir(profile_path)

    profile_names = dict()

    for profile in profile_folders:
        if exists(path_join(profile_path, profile, 'Editable.ini')):
            with open(path_join(profile_path, profile, 'Editable.ini')) as file:
                for line in file.readlines():
                    if len(line) > 12 and line[:12] == 'DisplayName=':
                        profile_names[line[12:-1]] = path_join(profile_path, profile)

    return profile_names


class StepManiaProfiles:
    """ Overview of Profiles

    .profile_names: Lists of all Profiles
    """

    step_mania_path = '../../'

    def __init__(self, step_mania_path: str = None):

        if step_mania_path is not None:
            self.step_mania_path = step_mania_path

        self.profile_names: dict = get_profile_names(path_join(self.step_mania_path, 'Save/LocalProfiles'))

        return

    def get_most_played_songs_overall(self) -> list:
        """ Return list with overview of all played songs"""

        song_dict_overall: dict = dict()

        for profile_name in self.profile_names:
            # this is somehow nasty nested
            song_list = StepManiaProfile(profile_name=profile_name, step_mania_path=self.step_mania_path
                                         ).get_most_played_songs()

            for song in song_list:
                if song[-1] in song_dict_overall:
                    # add times played.
                    song_dict_overall[song[-1]] += song[0]
                else:
                    song_dict_overall[song[-1]] = song[0]

        num_played = [(song_dict_overall[song], song) for song in song_dict_overall]
        return [(num, song) for num, song in sorted(num_played, reverse=True)]


class StepManiaProfile:
    """
    Load a StepMania Profile to extract some interesting information
    """

    profile_name = None
    profile_path = None
    _player_statistics = None
    _song_statistics = None

    def __init__(self, profile_name: str, step_mania_path: str = None):

        profiles = StepManiaProfiles(step_mania_path=step_mania_path)

        if profile_name in profiles.profile_names:
            self.profile_name = profile_name
            self.profile_path = profiles.profile_names[profile_name]
        else:
            raise Exception("Profile {profile} wasn't found. Available Profiles: {profiles}".format(
                profile=profile_name, profiles=profiles.profile_names))

        return

    ##############
    # Properties #
    ##############

    @property
    def player_statistics(self) -> dict:
        if self._player_statistics is None:
            self._player_statistics = get_player_statistics(path_join(self.profile_path, 'Stats.xml'))
        return self._player_statistics

    @property
    def song_statistics(self) -> dict:
        if self._song_statistics is None:
            self._song_statistics = get_song_statistics(path_join(self.profile_path, 'Stats.xml'))
        return self._song_statistics

    ###########
    # Methods #
    ###########

    def get_most_played_songs(self) -> list:
        """ Return list with overview of played songs: [(num_played, ... maybe more .... , song_name), ... ] """

        num_played = [(self.song_statistics[song]['NumTimesPlayed'], song) for song in self.song_statistics]
        return [(num, song) for num, song in sorted(num_played, reverse=True)]

    def get_preferred_songs_content(self) -> list:
        """ Returns a manually created list of preferred Songs.
        Note: This is not intended by StepMania
        """

        if exists(path_join(self.profile_path, 'SongManager PreferredSongs.txt')):
            with open(path_join(self.profile_path, 'SongManager PreferredSongs.txt')) as file:
                return file.readlines()
        else:
            return []


class StepManiaProfilePrints(StepManiaProfile):
    """ Commands to print out Statistics """

    def print_statistic(self) -> None:
        print('{name} has played {num_total} songs in {num_sessions} sessions:'.format(
            name=self.profile_name, num_total=self.player_statistics['NumTotalSongsPlayed'],
            num_sessions=self.player_statistics['TotalSessions']))
        print(' Game Time: {time_total:.1f} h ({time_game:.1f} h playing):'.format(
            time_total=self.player_statistics['TotalSessionSeconds'] / 60 / 60,
            time_game=self.player_statistics['TotalGameplaySeconds'] / 60 / 60))

        songs = {'Game Mode': 'NumSongsPlayedByPlayMode',
                 'Style:': 'NumSongsPlayedByStyle',
                 'Difficulty': 'NumSongsPlayedByDifficulty',
                 'Meter': 'NumSongsPlayedByMeter'}

        for entry in songs:
            print(' Songs played by {}:'.format(entry))
            for ent in self.player_statistics[songs[entry]]:
                print('  {name}: {value}'.format(name=ent, value=self.player_statistics[songs[entry]][ent]))
        print('\n')

    def print_ranking(self) -> None:
        most_played = self.get_most_played_songs()
        print('{name} has records of {num} rounds.'.format(name=self.profile_name,
                                                           num=sum([song[0] for song in most_played])))
        # todo: Are there records missing? Possible these from "Preferred"?
        self.print_ranking_general(most_played)
        print('\n')

    @staticmethod
    def print_ranking_general(most_played):
        max_played = max([song[0] for song in most_played])
        necessary_digits = int(np.floor(np.log10(max_played)))
        for rank, song in enumerate(most_played):
            print(('{rank:3d}. {num:'+str(necessary_digits)+'d}x {title}').format(
                rank=rank + 1, num=song[0], title=song[-1]))  # [-1] since maybe i will add more statistic later


if __name__ == '__main__':

    dance_profiles = StepManiaProfiles()

    print('\n')
    most_played_songs = dance_profiles.get_most_played_songs_overall()
    print('Most Played Songs Overall:')
    StepManiaProfilePrints.print_ranking_general(most_played_songs)
    print('\n')

    for profile in dance_profiles.profile_names:
        dance_profile = StepManiaProfilePrints(profile)
        dance_profile.print_statistic()
        dance_profile.print_ranking()

    print('\nDone.')
