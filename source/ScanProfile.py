from xml.etree import ElementTree as ET


def tag_text2dict(xml_child) -> dict:
    """ Scan child of XML-File and return it as dict. """

    content = dict()

    for group in xml_child:
        if group.text == '\n':
            content[group.tag] = tag_text2dict(group)
        else:
            content[group.tag] = group.text

    return content


def get_player_statistics(profile_path: str) -> dict:
    """
    :return: dict with information to all player.
    :param profile_path: path to profile folder
    """

    tree = ET.parse(profile_path)

    statistics = dict()

    extract_int = ['TotalSessions', 'TotalSessionSeconds', 'TotalGameplaySeconds', 'NumTotalSongsPlayed']
    extract_group_int = ['NumSongsPlayedByPlayMode', 'NumSongsPlayedByDifficulty', 'NumSongsPlayedByMeter']
    # NumSongsPlayedByStyle is handled below

    for part in tree.getroot():
        if part.tag == 'GeneralData':
            for data in part:

                if data.tag in extract_int:
                    statistics[data.tag] = int(data.text)

                elif data.tag in extract_group_int:
                    statistics[data.tag] = dict()
                    for entry in data:
                        statistics[data.tag][entry.tag] = int(entry.text)

                elif data.tag == 'NumSongsPlayedByStyle':
                    statistics[data.tag] = dict()
                    for entry in data:
                        statistics[data.tag][entry.attrib['Style']] = int(entry.text)

    return statistics


def get_song_statistics(profile_path: str) -> dict:
    """
    :return: dict with information to all played songs.
    :param profile_path: path to profile folder
    """

    tree = ET.parse(profile_path)

    song_list = dict()

    for part in tree.getroot():
        if part.tag == 'SongScores':
            for songs in part:

                song_list[songs.attrib['Dir'][6:-1]] = {
                    'Difficulty': songs[0].attrib['Difficulty'],
                    'StepsType': songs[0].attrib['StepsType'],
                    'NumTimesPlayed': int(songs[0][0][0].text),
                    'LastPlayed': songs[0][0][1].text,
                    'Score': tag_text2dict(songs[0][0][3]) if len(songs[0][0]) > 2 else None
                }

                # score = songs[0][0][3][2].text
                # percent = songs[0][0][3][3].text
                # survive_seconds = songs[0][0][3][4].text
                # max_combo = root[1][3][0][0][3][5].text
                # date_time = root[1][3][0][0][3][9].text
    return song_list


if __name__ == '__main__':

    # expecting the Repository in your StepMania 5 folder
    profile_path = '../../Save/LocalProfiles/00000000/Stats.xml'

    # Get info to Player
    player_stats = get_player_statistics(profile_path)
    print('Overview of statistics for one Profile:')
    for sta in player_stats:
        print(' {sta}: {value}'.format(sta=sta, value=player_stats[sta]))
    print('\n')

    # Get info to all played songs for one Player
    song_li = get_song_statistics(profile_path)
    print('Overview of Song information for one Profile:')
    for song in sorted(list(song_li)):
        name = song if len(song) < 50 else song[:46] + '...'
        print(' {name} {indent}{song}'.format(name=name,
                                              indent=' '*max(50-len(name), 0),
                                              song=song_li[song]))
    print('\n')

    print('\nDone')
