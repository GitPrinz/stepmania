from os.path import exists, join as path_join

from source.Profile import StepManiaProfile, StepManiaProfiles
from source.ScanSongs import get_songs_with_video


class StepManiaSongList:
    """
    Handle Preferred Songs.

    Use
        .add_manual_list(list_path)
            .add_profile_preferred(profile_name)
        .add_most_played()
        .add_profile_most_played(profile_name)
        .add_songs_with_video()
    to create your overview.

    Note: Songs can only appear once and will be ignored if they should be added a second time.

    Checkout .list_draft to see the result.
    Use .write() to replace your old List (There is no overwrite protection).

    """

    list_path = None
    list_draft = []  # todo: list draft as dict with blocks

    def __init__(self, game_path: str = 'C:/Games/StepMania 5', step_mania_path: str = None):

        self.game_path = game_path
        self.step_mania_path = step_mania_path
        self.list_path = path_join(game_path, 'Themes/default/Other/SongManager PreferredSongs.txt')
        if not exists(self.list_path):
            raise Exception('Path to PreferredSongs not found: {}'.format(self.list_path))

        return

    def add_list_manual(self, list_path: str) -> None:
        print('Add list: "{}"'.format(list_path))
        if exists(list_path):
            with open(list_path) as file:
                self.list_draft.extend(file.readlines())
        else:
            raise Exception("File wasn't found: {}".format(list_path))

    def add_profile_preferred(self, profile_name: str) -> None:
        """ To include a manual list. """
        print("Add {}'s Songs List".format(profile_name))
        profile = StepManiaProfile(profile_name=profile_name, step_mania_path=self.step_mania_path)
        self.list_draft.extend(profile.get_preferred_songs_content())
        return

    def add_profile_most_played(self, profile_name: str) -> None:
        print("Add {}'s most played Songs".format(profile_name))
        profile = StepManiaProfile(profile_name=profile_name, step_mania_path=self.step_mania_path)
        self.list_draft.append("---{}'s most played".format(profile_name))
        self.list_draft.extend([song[-1] for song in profile.get_most_played_songs()])
        return

    def add_most_played(self,) -> None:
        print("Add overall most played Songs")
        self.list_draft.append("---Most played songs")
        self.list_draft.extend([song[-1] for song in StepManiaProfiles(step_mania_path=self.step_mania_path
                                                                       ).get_most_played_songs_overall()])
        return

    def add_songs_with_video(self):
        print("Add Songs with Video")
        self.list_draft.append('---Songs with Video')
        self.list_draft.extend(get_songs_with_video('../../Songs'))

    def load(self) -> None:
        with open(self.list_path) as file:
            self.list_draft = file.readlines()

    def write(self) -> None:
        print("Write Song List to StepMania Directory.")

        line_buffer = []

        for line in self.list_draft:

            # prepare line
            #   don't draw empty
            #   add line break
            if line is None or len(line) < 1:
                continue
            elif line[-1] != '\n':
                line = line + '\n'

            # don't write duplicates
            if line in line_buffer:
                if len(line) > 3 and line[:3] == '---':
                    raise Exception('Multiple Blocks with same name in SongList: "{}"'.format(line))
                continue

            line_buffer.append(line)

        with open(self.list_path, 'w') as file:
            file.writelines(line_buffer)

        return


if __name__ == '__main__':

    song_list = StepManiaSongList()

    # Add manual created Song list.
    # Remove this or create a preferred Song list in your Profile
    song_list.add_profile_preferred('DancePrinz')

    # Add Most Played Song from a Profile.
    song_list.add_most_played()

    # Add all Songs with Video.
    song_list.add_songs_with_video()

    # Save the Profile (Will overwrite existing)
    song_list.write()
