from os import listdir, replace
from os.path import isdir, join

list_path = 'C:/Games/StepMania 5/Themes/default/Other/SongManager PreferredSongs.txt'
exclude_dirs = ['1Hard - (maybe add later)', '2Hard - excluded']


def get_preferred_song_list() -> list:

    with open(list_path) as file:
        song_list = file.readlines()

    return song_list


def get_songs_with_video(song_path: str = 'Songs') -> list:
    """ Get all songs with and ".avi" file in there folder """

    # https://mkyong.com/python/python-how-to-list-all-files-in-a-directory/

    songs = set()
    album_dirs = listdir(song_path)

    for a_dir in album_dirs:
        if a_dir in exclude_dirs:
            continue
        if not isdir(join(song_path, a_dir)):
            continue

        song_dirs = listdir(join(song_path, a_dir))
        for s_dir in song_dirs:
            if not isdir(join(song_path, a_dir, s_dir)):
                continue

            files = listdir(join(song_path, a_dir, s_dir))
            for file in files:
                if '.avi' in file:
                    songs.add('{a}/{s}'.format(a=a_dir, s=s_dir))

    return sorted(list(songs))


def update_songs_with_video():
    """ Add all songs with video to Preferred Songs"""

    song_list = get_preferred_song_list()
    video_songs = get_songs_with_video()

    # remove video songs
    song_list_wo_video = []
    topic = ''

    for line in song_list:
        if len(line) < 3:
            song_list_wo_video.append(line)
            continue
        elif line[:3] == '---':
            topic = line[3:-1]  # '/n' at the end (one character)
            song_list_wo_video.append(line)
        elif topic == 'Video':
            continue
        else:
            song_list_wo_video.append(line)

    # remove duplicates
    video_songs = [line for line in video_songs if "{}\n".format(line) not in song_list_wo_video]

    with open(list_path + '.tmp', "w") as file:

        video_written = False
        topic = ''

        for line in song_list:
            if len(line) < 3:
                file.write(line)
                continue
            elif line[:3] == '---':
                topic = line[3:-1]  # '/n' at the end (one character)
                file.write(line)
            elif topic == 'Video':
                if not video_written:
                    file.writelines("{}\n".format(l) for l in video_songs)
                    video_written = True
            else:
                file.write(line)

    return


def sort_region(region_name: str):
    """ Sort all Songs in Preferred Songs """

    song_list = get_preferred_song_list()

    with open(list_path + '.tmp', "w") as file:

        buffer = []
        topic = ''

        for line in song_list:
            if len(line) < 3:
                file.write(line)
                continue
            elif line[:3] == '---':
                if len(buffer) > 0:
                    file.writelines(sorted(buffer))
                    buffer = []
                topic = line[3:-1]  # '/n' at the end (one character)
                file.write(line)
            elif topic == region_name:
                buffer.append(line)
            else:
                file.write(line)

        # in case region is at the end
        if len(buffer) > 0:
            file.writelines(sorted(buffer))

    return


def clean_and_save():
    """ Remove Duplicates from Preferred Songs. """

    with open(list_path + '.tmp') as file:
        song_list = file.readlines()

    unique_list = []
    for line in song_list:
        if line not in unique_list:
            unique_list.append(line)
        else:
            print('Removed Duplicate: {}'.format(line[:-1]))

    with open(list_path + '.tmp', "w") as file:
        file.writelines(unique_list)

    replace(list_path + '.tmp', list_path)


if __name__ == '__main__':

    sort_region("Martin's Favorites")

    update_songs_with_video()

    clean_and_save()
